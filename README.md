# lispy
## tiny lisp engine in C
**(after http://buildyourownlisp.com)**

* Needs: libedit-dev, mpc.c & mpc.h (from https://github.com/orangeduck/mpc)
* Compile by: `cc -Wall -std=c99 lispy.c mpc.c -lm -ledit -o lispy`

Additions to the original project:
- Ctrl-D on an empty REPL line exits instead of segfaults
- Typing `exit` to exit works
- Responds `OK` instead of `()`
- Lines can be continuated by ending them with `/`
- Functions `%`, `^`, `min` and `max` predefined
- `--debug` option can be given to output parsing info in REPL or the files that come after it on the commandline.
- `--batch` option bypasses REPL, only processes files on the commandline.
